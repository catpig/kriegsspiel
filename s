#!/usr/bin/env bash
# This is a convenience script for development and also serves to show a possible long startup command.
./src/start.py --server --home_directory src --asset_directory ./asset_packs/default
