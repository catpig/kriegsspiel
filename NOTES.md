# NOTES #

### Longterm ToDo's ###

* this for a start

### other Todo's ###

* marked by 'TODO' in a comment and/or listed in doc/todo.txt
* really important stuff is marked by 'XXX'
* others: 'FIXME', 'NOTE', 'DEBUG'

### Organisation ###

* assets consist of names, descriptions and properties in a yaml file and any required graphics and sound files
* all assets are managed by an asset manager
* assets are accessed only through the asset manager
* each asset type is implemented by one or more classes
* all assets are divided in the following types:
    * maps and/or settings/instructions for a map generator
    * (in future) scenarios and/or campaigns
    * countries
    * terrains
    * units
    * buildings (includes any improvements that can be built and/or destroyed during a game)
    * sounds
* assets are placed in folders named according to their type
* the default assets are located in /mods/default
* assets added by mods are located in /mods/<name of the mod pack>
    * namespace in this directory is expected to be used cooperatively - ie. just don't use a name that is already used by someone else
    * mods must include a metadata file (details to be determined)
