# README #

### General ###

* originally intended to be a modern implementation of [Kriegsspiel](https://en.wikipedia.org/wiki/Kriegsspiel_(wargame))
* turn-based strategy game without a tile-based map and complex economics and combat
 
### Setup ###

* client requires numpy and pyglet for now (will likely switch to panda3d)
* server requires qt5 for now (will likely switch to something else)

### Copying/License ###
wiab is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See doc/agpl_3.0.txt for full details.
