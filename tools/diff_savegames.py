#!/usr/bin/env python3.5
# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.


# import std lib
import os
import sys

# other libs
import msgpack

# own modules


def init():
	if len(sys.argv) != 3:
		print("This program is used to compare two savegames. Please provide exactly two filenames as parameters.")
	
	if not os.path.exists(sys.argv[1]):
		print("can't load, file %s doesn't exist" % sys.argv[1])
		return
	
	with open(sys.argv[1], 'rb') as file_a:
		save_a = msgpack.unpack(file_a)
	
	with open(sys.argv[2], 'rb') as file_b:
		save_b = msgpack.unpack(file_b)
	
	for key in (b'asset_path', b'next_thing_id', b'players', b'size'):
		if save_a[key] != save_b[key]:
			print("Differing %s: '%s' and '%s'" % (key, str(save_a[key]), str(save_b[key])))
	
	for key in (b'players', b'things'):
		if len(save_a[key]) != len(save_b[key]):
			print("Differing amount of %s: '%d' and '%d'" % (key, len(save_a[key]), len(save_b[key])))
		else:
			for something_id in save_a[key].keys():
				if something_id not in save_b[key].keys():
					print("ID %d missing in second save" % something_id)
				else:
					for inner_key in save_a[key][something_id].keys():
						if save_a[key][something_id][inner_key] != save_b[key][something_id][inner_key]:
							print("Differing %s for %s_id %d: '%s' and '%s'" % (inner_key, key, something_id, str(save_a[key][something_id][inner_key]), str(save_b[key][something_id][inner_key])))


if __name__ == '__main__':
	init()
