#!/usr/bin/env python3.5
# -*- coding: utf8 -*-

from direct.showbase.ShowBase import ShowBase

print("This file is a very simple panda test that does nothing but show an empty window.")


class TestApp(ShowBase):
	def __init__(self):
			super().__init__()

APP = TestApp()
APP.run()
