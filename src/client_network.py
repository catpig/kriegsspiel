# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
import socket
import logging
import time
from typing import Dict, Callable

# other libs
import msgpack
from direct.task.Task import Task

# own modules


class ClientNetwork:
	"""
	Handles the network connection for the client.
	
	It is based on emitters which are basically strings to listen to and a function to call if that string is detected.
	"""
	
	def __init__(self, config):
		self.config = config
		self.log = logging.getLogger('wiab.client.network')
		
		self.data_to_process = b''
		
		self.emitters = {}
		self.socket = socket.create_connection((self.config.network.host, config.network.port))
		self.socket.setblocking(False)
	
	def poll_on_network(self, task: Task) -> None:  # pylint: disable=unused-argument
		"""
		Polls the socket for new received data. Called repeatedly by the panda task manager.
		
		:param task: task object of panda3d's task manager
		"""
		time_stop = time.time() + self.config.client.network_poll_runtime
		
		while time.time() < time_stop:
			if not self._read(4096):
				break
			
			while True:
				if len(self.data_to_process) < 4:
					break  # length field not completely available
				
				length = int.from_bytes(self.data_to_process[:4], byteorder='big')
				
				if len(self.data_to_process) < length + 4:
					break  # message not completely available
				
				message = msgpack.unpackb(self.data_to_process[4:4 + length])
				if self.config.general.debug_verbose:
					self.log.debug('received the following message: ' + str(message))
				
				self.emitters[message[0]](message)
				
				self.data_to_process = self.data_to_process[4 + length:]  # remove the processed data from the buffer
		
		return Task.cont  # signal the panda task manager to continue calling the task
	
	def _read(self, bytes_to_read: int) -> bool:
		try:
			new_data = self.socket.recv(bytes_to_read)
		except BlockingIOError:
			return False
		if len(new_data) == 0:
			return False
		self.data_to_process += new_data
		return True
	
	def send(self, *args):
		message = msgpack.packb(args)
		length = len(message).to_bytes(4, byteorder="big")
		self.socket.sendall(length + message)
	
	def quit(self):  # TODO: this needs to actually be called
		self.socket.close()
	
	def set_emitters(self, emitters: Dict[bytes, Callable]):
		self.emitters = emitters
	
	def add_emitter(self, name: str, function: Callable):
		self.emitters[name] = function
	
	def remove_emitter(self, name: str):
		del self.emitters[name]
