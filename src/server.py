# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
import sys
import logging

# pyqt
from PyQt5.QtWidgets import QApplication
from PyQt5.QtNetwork import QHostAddress, QTcpServer

# other libs
import msgpack

# own modules
from server_game import ServerGame


class Server(QApplication):
	def __init__(self, config):
		super(Server, self).__init__(sys.argv)
		
		self.log = logging.getLogger('wiab.server')
		self.log.info('starting server')
		
		self.config = config
		
		self.emitters = {b"build_c2s": self.received_build,
		                 b"connect_c2s": self.received_connect,
		                 b"end_turn_not_ready_c2s": self.received_end_turn_not_ready,
		                 b"end_turn_ready_c2s": self.received_end_turn_ready,
		                 b"move_c2s": self.received_move,
		                 b"start_new_game_c2s": self.received_start_new_game,
		                 b"quick_load_and_start_c2s": self.received_quick_load_and_start,
		                 b"quick_save_c2s": self.received_quick_save}  # TODO I think most of these should actually directly call game
		
		self.data_to_process = b''
		
		self.client_socket_dicts = {}  # player_id:{"socket":QTcpSocket, "id":int, "player_name":str}
		self.tcp_server = QTcpServer()
		self.tcp_server.newConnection.connect(self.new_connection)
		self.tcp_server.listen(QHostAddress("127.0.0.1"), 49000)
		
		self.game = None
	
	def new_connection(self):
		while self.tcp_server.hasPendingConnections():
			new_client_socket = self.tcp_server.nextPendingConnection()
			# TODO new_client_socket.disconnected.connect(self.client_socket_disconnected)
			new_client_socket.readyRead.connect(self.on_ready_read)
			self.log.info("connected %s", new_client_socket.peerAddress())
	
	def on_ready_read(self, sender=None, recursively_called=False):
		if not recursively_called:
			self.data_to_process += self.sender().readAll().data()
			sender = self.sender()
		
		if len(self.data_to_process) < 4:
			return  # didnt receive length field yet
		
		length = int.from_bytes(self.data_to_process[:4], byteorder='big')
		
		if len(self.data_to_process) < length:
			return  # didnt receive full message yet
		
		message = msgpack.unpackb(self.data_to_process[4:4 + length])
		
		self.emitters[message[0]](sender, message)
		
		self.data_to_process = self.data_to_process[4 + length:]
		if len(self.data_to_process) > 0:
			self.on_ready_read(sender=sender, recursively_called=True)
	
	def received_build(self, sender, message):
		new_thing_id = self.game.make_and_place_new_thing(self.socket_to_player_id(sender), self.game.things[message[1]].location, message[2].decode("UTF8"))
		self.game.things[new_thing_id].send(self.send_all)  # inform clients about the new thing
	
	def received_connect(self, sender, message):
		player_name = message[1].decode("UTF8")
		for socket_dict in self.client_socket_dicts.values():
			if socket_dict["player_name"] == player_name:  # reconnect
				self.client_socket_dicts[socket_dict["player_id"]]["socket"] = sender
				self.send_player_info_to_all()
				self.log.info("%s reconnected", player_name)
				return
		
		if self.game:
			self.log.warning("cannot connect new player to running game")  # TODO send error to client
			return
		
		if len(self.client_socket_dicts.items()) > 0:
			player_id = max(self.client_socket_dicts.keys()) + 1
		else:
			player_id = 1  # start at since id 0 is used for unknown
		self.client_socket_dicts[player_id] = {"socket": sender, "player_name": player_name, "player_id": player_id}
		self.send_player_info_to_all()
		self.log.debug("recorded socket for new player %s with id %d", player_name, player_id)
	
	def received_end_turn_not_ready(self, sender, message):  # this also starts end_turn (if everyone is ready), param message is intentionally unused # pylint: disable=unused-argument
		self.game.players[self.socket_to_player_id(sender)].end_turn_ready = False
	
	def received_end_turn_ready(self, sender, message):  # this also starts end_turn (if everyone is ready), param message intentionally unused # pylint: disable=unused-argument
		self.game.players[self.socket_to_player_id(sender)].end_turn_ready = True
		self.send_all("end_turn_ready_s2c", self.socket_to_player_id(sender))
		
		for player in self.game.players.values():
			if player.human and not player.end_turn_ready:
				return  # a player is not ready, so we don't proceed with starting end_turn
		self.send_all("end_turn_start_s2c")
		
		self.game.end_turn(self.send_all)
		
		self.send_all("end_turn_done_s2c")
	
	def received_move(self, sender, message):
		self.game.move(message[1], message[2], self.socket_to_player_id(sender))  # move thing
		thing = self.game.things[message[1]]
		thing.send(self.send_all)  # inform clients about the new values of the thing
	
	def received_quick_load_and_start(self, sender, message):  # param(s) intentionally unused # pylint: disable=unused-argument
		self.game = ServerGame(self.config, None, self.client_socket_dicts, True)
		self.game.quick_load()
		self.send_all("start_new_game_s2c", self.game.size)
		self.send_full_game_info_to_all()
	
	def received_quick_save(self, sender, message):  # param(s) intentionally unused # pylint: disable=unused-argument
		self.game.quick_save()
	
	def received_start_new_game(self, sender, message):  # param(s) intentionally unused # pylint: disable=unused-argument
		self.game = ServerGame(self.config, message[1], self.client_socket_dicts)
		self.send_all("start_new_game_s2c", self.game.size)
		self.send_full_game_info_to_all()
	
	def send_all(self, *args):
		message = msgpack.packb(args)
		length = len(message).to_bytes(4, byteorder="big")
		for socket_dict in self.client_socket_dicts.values():
			socket_dict["socket"].write(length + message)
			socket_dict["socket"].flush()
	
	def send_full_game_info_to_all(self):
		self.send_all("clear_things_s2c")
		self.send_all("current_turn_s2c", self.game.current_turn)
		for player_id, player in self.game.players.items():
			if player.human:
				self.send_one(player_id, "player_storage_s2c", player.storage.volume_per_item, player.storage.volume_total, player.storage.contents)
		for thing in self.game.things.values():
			thing.send(self.send_all)
	
	def send_player_info_to_all(self):
		for socket_dict in self.client_socket_dicts.values():
			self.send_all("new_player_s2c", socket_dict["player_id"], socket_dict["player_name"])
	
	def send_one(self, player_id, *args):
		message = msgpack.packb(args)
		length = len(message).to_bytes(4, byteorder="big")
		self.client_socket_dicts[player_id]["socket"].write(length + message)
		self.client_socket_dicts[player_id]["socket"].flush()
	
	def socket_to_player_id(self, socket):
		for socket_dict in self.client_socket_dicts.values():
			if socket_dict["socket"] == socket:
				return socket_dict["player_id"]
