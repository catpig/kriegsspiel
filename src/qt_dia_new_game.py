# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib

# pyqt
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtWidgets import QDialog, QGridLayout, QLabel, QLineEdit, QPushButton

# other libs

# own modules


class QtDiaNewGame(QDialog):
	new_game_clicked = pyqtSignal(int, int)
	
	def __init__(self, parent):
		super(QtDiaNewGame, self).__init__(parent)
		
		self.setWindowModality(Qt.WindowModal)
		self.setWindowTitle("Connect")
		self.grid = QGridLayout(self)
		
		row_count = 0
		column_count = 0
		
		self.grid.addWidget(QLabel("X size"), row_count, column_count)
		column_count += 1
		
		self.edit_size_x = QLineEdit("32")
		self.grid.addWidget(self.edit_size_x, row_count, column_count)
		column_count += 1
		
		self.grid.addWidget(QLabel("Y size"), row_count, column_count)
		column_count += 1
		
		self.edit_size_y = QLineEdit("32")
		self.grid.addWidget(self.edit_size_y, row_count, column_count)
		row_count += 1
		column_count = 0
		
		button_cancel = QPushButton("Cancel")
		button_cancel.clicked.connect(self.close)
		self.grid.addWidget(button_cancel, row_count, column_count)
		column_count += 1
		
		button_connect = QPushButton("Start Game")
		button_connect.clicked.connect(self.start_clicked)
		self.grid.addWidget(button_connect, row_count, column_count)
		button_connect.setDefault(True)
	
	def start_clicked(self):
		size_x = int(self.edit_size_x.text())
		size_y = int(self.edit_size_y.text())
		
		self.new_game_clicked.emit(size_x, size_y)
		self.close()
