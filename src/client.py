# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# std lib
import logging

# import panda3d
from direct.showbase.ShowBase import ShowBase

# own modules
from client_game import ClientGame
from client_network import ClientNetwork
from client_camera import ClientCamera
from client_world import ClientWorld
from player import Player


class Client(ShowBase):
	"""
	This is the starting point for the rest of the client.
	
	It instantiates the classes for network, world, pyglet (window) and game.
	"""
	
	def __init__(self, config):
		super().__init__()
		self.config = config
		
		self.log = logging.getLogger('wiab.client')
		
		self.player = Player(self.config.client.player_name)
		self.network = ClientNetwork(self.config)
		self.world = ClientWorld(self.config, self.render)
		self.game = ClientGame(self.config, self.network, self.world, size=None, assets=None, player=self.player)
		self.wiab_camera = ClientCamera(self.config, self.camera)
		
		self.log.debug('all classes instantiated')
		
		# disable default camera controls
		self.disable_mouse()
		
		# register events
		self._register_events()
		
		# add tasks
		self.taskMgr.add(self.wiab_camera.update, 'camera_update')
		self.taskMgr.add(self.world.update, 'world_update')
		# TODO: make this async if possible (not possible with panda event loop but we could use another event loop)
		self.taskMgr.add(self.network.poll_on_network, 'network_poll')
		
		# XXX: simple gui with debug text
		# TODO: add mouse control for the camera
		
		self.selected_thing_id = None

	def _register_events(self):
		# this is useful to debug all kinds of events but generates a lot of shell noise
		# self.messenger.toggleVerbose()
		
		self.accept('1', self.move, [-0.707, -0.707])
		self.accept('2', self.move, [0, -1])
		self.accept('3', self.move, [0.707, -0.707])
		self.accept('4', self.move, [-1, 0])
		self.accept('5', self.cancel_action_points)
		self.accept('6', self.move, [1, 0])
		self.accept('7', self.move, [-0.707, 0.707])
		self.accept('8', self.move, [0, 1])
		self.accept('9', self.move, [0.707, 0.707])
		
		self.accept('a', self.wiab_camera.change_movement, [0, -1])
		self.accept('a-up', self.wiab_camera.change_movement, [0, 1])
		self.accept('d', self.wiab_camera.change_movement, [0, 1])
		self.accept('d-up', self.wiab_camera.change_movement, [0, -1])
		self.accept('e', self.wiab_camera.change_height, [1])
		self.accept('l', self.build, ['lumberjack_hut'])  # TODO on this and others: different hotkey meaning depending on selected unit
		self.accept('m', self.build, ['sawmill'])
		self.accept('q', self.wiab_camera.change_height, [-1])
		self.accept('s', self.wiab_camera.change_movement, [1, 0])
		self.accept('s-up', self.wiab_camera.change_movement, [-1, 0])
		self.accept('w', self.wiab_camera.change_movement, [-1, 0])
		self.accept('w-up', self.wiab_camera.change_movement, [1, 0])
		
		self.accept('arrow_down', self.wiab_camera.change_orientation_keyboard, [0, -1])
		self.accept('arrow_left', self.wiab_camera.change_orientation_keyboard, [1, 0])
		self.accept('arrow_right', self.wiab_camera.change_orientation_keyboard, [-1, 0])
		self.accept('arrow_up', self.wiab_camera.change_orientation_keyboard, [0, 1])
		self.accept('backspace', self.network.send, ['end_turn_not_ready_c2s'])
		self.accept('enter', self.network.send, ['end_turn_ready_c2s'])
		self.accept('space', self.select_actionable_thing)
		
		self.accept('f2', self.network.send, ['quick_save_c2s'])
		self.accept('f3', self.network.send, ['quick_load_and_start_c2s'])
		# f as fullscreen and trapped mouse
		self.accept('escape', quit)  # TODO use proper shutdown path, inform server
	
	def build(self, thing_type_name: str):
		if self.selected_thing_id is not None:
			self.network.send("build_c2s", self.selected_thing_id, thing_type_name)
		else:
			self.log.warning("You need to select a thing to be able to build something.")
	
	def cancel_action_points(self):  # This is intentionally client-side only. The point is to let the user skip the currently selected thing for select_actionable_thing.
		if self.selected_thing_id is not None:
			self.game.things[self.selected_thing_id].action_points = 0.0
			self.select_actionable_thing()
		else:
			self.log.warning("You need to select a thing to be able to cancel action points.")
	
	def move(self, delta_x, delta_y):  # TODO handle if insufficient action_points are left (taking into account movement_speed)
		if self.selected_thing_id is not None:
			self.network.send('move_c2s', self.selected_thing_id, (self.game.things[self.selected_thing_id].location[0] + delta_x, self.game.things[self.selected_thing_id].location[1] + delta_y))
		else:
			self.log.warning("You need to select a thing to move.")
	
	def select_actionable_thing(self):  # TODO this currently just selects any actionable thing, instead it should rotate through them in some order
		for thing in self.game.things.values():
			if thing.action_points > 0.0 and thing.owner_id == self.game.player.player_id:
				self.selected_thing_id = thing.thing_id
				self.log.debug("selected thing: %s", str(thing.dict_for_save()))
				return
		# self.selected_thing_id = None
		self.log.warning("Nothing available that has any action points left.")
