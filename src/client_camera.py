# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.


# import builtins
import logging
import time

# import other libs
import numpy
from direct.task.Task import Task

# import own
from helpers import rotate_vector, sgn


class ClientCamera:
	def __init__(self, config, camera):
		self.log = logging.getLogger('wiab.client.camera')
		
		self.conf = config
		self.camera = camera  # panda camera
		
		self._last_update = None
		
		# position (x, y, z) relative to the world with orientation = (0, 0, 0)
		# x: right
		# y: forward
		# z: upwards
		self.position = (0.0, 0.0, 25.0)
		
		# orientation
		# 1: heading/yaw (z axis)
		# 2: pitch (forward/backward rotation)
		# 3: roll (sideways rotation)
		self.orientation = (0.0, 0.0, 0.0)
		
		# the speed and acceleration in direction of the x and y axes
		self.speed = (0.0, 0.0)
		self.acceleration = (0.0, 0.0)
		
		# movement orders relative to the camera
		# first element: 1 backwards, -1 forward; second element: 1 right, -1 left
		self._movement = [0, 0]
		
		# the following two are for time controlled height changes
		self.move_target_time = 0
		self.move_target_height = self.position[2]
		
		self.orientation_target = self.orientation
		self.orientation_target_time = 0
	
	def update(self, task):
		"""
		Called repeatedly by the panda task manager and triggers updates for camera position.
		
		@:param task: the task object passed py the panda task manager, see https://www.panda3d.org/manual/index.php/Tasks for details
		"""
		
		if self._last_update is not None:
			dt = task.time - self._last_update
			dt = min(dt, self.conf.client.update_time_limit)
		else:
			dt = 0
		
		self._last_update = task.time
		
		self.update_speed(dt)
		self.update_position(dt)
		self.update_height(dt)
		self.update_orientation(dt)
		
		self.camera.setPos(*self.position)
		self.camera.setHpr(*self.orientation)  # HPR: Heading/Yaw, Pitch, Roll
		
		return Task.cont
	
	def update_speed(self, dt):
		# TODO: there may be a bug in here. deceleration is behaving a bit weird when turning while moving
		# v = a * dt + v0
		# rotate self.speed to match the camera direction
		v0 = rotate_vector(self.speed, -1 * self.orientation[0])
		
		# calculate acceleration from self.movement relative to the camera
		a = numpy.multiply((self._movement[1], -1 * self._movement[0]), self.conf.client.move_acceleration)
		
		# calculate new speed (vn)
		vn = numpy.multiply(a, dt)
		vn = numpy.add(vn, v0)
		
		# check if a is 0 on any axis, decelerate on this axis if so
		for i in range(len(a)):
			if a[i] == 0:
				vn[i] += -1 * sgn(vn[i]) * dt * self.conf.client.move_deceleration
				if sgn(v0[i]) + sgn(vn[i]) == 0:
					vn[i] = 0.0
		
		# limit speed to terminal velocity
		if numpy.linalg.norm(vn) > self.conf.client.move_terminal_velocity:
			l = numpy.linalg.norm(vn)
			vn = numpy.divide(vn, l)
			vn = numpy.multiply(vn, self.conf.client.move_terminal_velocity)
		
		# convert speed back relative to the world
		self.acceleration = rotate_vector(a, self.orientation[0])
		self.speed = rotate_vector(vn, self.orientation[0])
	
	def update_position(self, dt):
		# p = a / 2 * dt**2 + v0 * t + p0
		# len(self.position) is 3, len() of self.speed and self.acceleration is 2
		p0 = self.position[0:2]  # get only x and z
		p = numpy.divide(self.acceleration, 2)
		p = numpy.multiply(p, dt ** 2)
		p2 = numpy.multiply(self.speed, dt)
		p = numpy.add(p, p2)
		p = numpy.add(p, p0)
		self.position = (p[0], p[1], self.position[2])
	
	def update_height(self, dt):
		time_remain = self.move_target_time - time.time()
		if time_remain > 0:
			height_remain = self.move_target_height - self.position[2]
			dh = numpy.multiply(height_remain, dt / time_remain)
			if abs(height_remain) < abs(dh):
				dh = height_remain
			x, y, z = self.position
			self.position = (x, y, z + dh)
		elif self.position[2] != self.move_target_height:
			x, y, z = self.position
			self.position = (x, y, self.move_target_height)
	
	def update_orientation(self, dt):
		"""
		do a smooth transition to the target orientation
		:param dt:
		:return:
		"""
		time_remain = self.orientation_target_time - time.time()
		if time_remain > 0:
			o_remain = numpy.subtract(self.orientation_target, self.orientation)
			do = numpy.multiply(o_remain, dt / time_remain)
			if numpy.linalg.norm(o_remain) < numpy.linalg.norm(do):
				do = o_remain
			self.orientation = numpy.add(do, self.orientation)
		else:
			self.orientation = self.orientation_target
	
	def change_movement(self, x, y):
		"""
		Simple helper to give an easier to use interface to self._movement.
		"""
		self._movement[0] += x
		self._movement[1] += y
	
	# simple function to change the height by a defined number of steps
	def change_height(self, amount):
		# reset target height if there is no current change happening
		if time.time() > self.move_target_time:
			self.move_target_height = self.position[2]
		self.move_target_height += amount * self.conf.client.move_height_change_distance
		self.move_target_time = time.time() + self.conf.client.move_height_change_time
	
	def change_orientation(self, amount_x, amount_y, amount_z=0, smooth=False):
		if smooth:
			if time.time() > self.orientation_target_time:
				self.orientation_target = self.orientation
			self.orientation_target = numpy.add(self.orientation_target, (amount_x, amount_y, amount_z))
			self.orientation_target_time = time.time() + self.conf.client.orientation_change_time
		else:
			self.orientation_target = numpy.add(self.orientation_target, (amount_x, amount_y, amount_z))
	
	def change_orientation_keyboard(self, dx, dy):
		self.change_orientation(dx * self.conf.client.orientation_change_arrows,
		                        dy * self.conf.client.orientation_change_arrows,
		                        smooth=True)
	
	def change_orientation_mouse(self, dx, dy):
		self.change_orientation(dx * self.conf.client.orientation_change_mouse,
		                        dy * self.conf.client.orientation_change_mouse)
