# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
import math
from typing import Tuple

# other libs

# own modules
from assets import Assets
from model_storage import ModelStorage
from player import Player


class ModelGame:
	"""
	Saves all the important info needed for the game.
	
	Children of ModelGame have to implement a property to access size!
	"""
	
	def __init__(self, size: [Tuple[float, float], None], assets: [Assets, None]):
		self.current_turn = 0
		
		self._size = size  # subclasses have to implement a way to expose self._size as self.size!
		self.things = {}  # thing_id:ModelThing
		
		self.players = {}  # player_id:Player
		self.players[0] = Player("Unowned", 0)
		
		self.assets = assets
	
	def end_turn(self):
		self.current_turn += 1
		self.log.debug("ModelGame.end_turn current_turn=%d", self.current_turn)
		for player in self.players.values():
			player.end_turn()
	
	def get_height(self, location: Tuple[float, float]) -> float:
		# TODO: implement a model to use and save terrain data
		x, y = location
		# see this link for a coloured 3D plot: http://www.wolframalpha.com/input/?i=3d+plot&rawformassumption=%7B%22F%22,+%223DPlot%22,+%223dplotfunction%22%7D+-%3E%22sin(x)+-+cos(y)+%2B+sin(x%2B1)%22&rawformassumption=%7B%22FVarOpt%22%7D+-%3E+%7B%7B%223DPlot%22,+%223dplotvariable1%22%7D,+%7B%223DPlot%22,+%223dplotlowerrange1%22%7D,+%7B%223DPlot%22,+%223dplotupperrange1%22%7D,+%7B%223DPlot%22,+%223dplotvariable2%22%7D,+%7B%223DPlot%22,+%223dplotlowerrange2%22%7D,+%7B%223DPlot%22,+%223dplotupperrange2%22%7D%7D&rawformassumption=%7B%22C%22,+%223d+plot%22%7D+-%3E+%7B%22Calculator%22%7D&rawformassumption=%7B%22MC%22,%22%22%7D-%3E%7B%22Formula%22%7D # noqa pylint: disable=line-too-long
		return 10 * (math.sin(10 * x) - math.cos(10 * y) + math.sin(10 * (x + 1)))
	
	def start_game(self):
		for player_a in self.players.values():
			# setup storage
			player_a.storage = ModelStorage(self.assets.other['player_storage']['volume_per_item'], self.assets.other['player_storage']['volume_total'])
			
			# setup diplomatic states
			for player_b in self.players.values():
				if player_a != player_b:
					player_a.diplomatic_states[player_b.player_id] = "peace"  # only need to do it in one direction since we get to the reverse later in the loop
