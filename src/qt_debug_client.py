# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
import logging
import sys

# pyqt
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QKeySequence
from PyQt5.QtNetwork import QTcpSocket
from PyQt5.QtWidgets import QMainWindow, QMenu, QMenuBar, QStatusBar, QTabWidget

# other libs
import msgpack

# own modules
from assets import Assets
from model_storage import ModelStorage
from model_thing import ModelThing
from qt_client_game import QtClientGame
from qt_dia_connect import QtDiaConnect
from qt_dia_new_game import QtDiaNewGame
from qt_tab_map import QtTabMap
from qt_tab_players import QtTabPlayers
from qt_tab_things import QtTabThings


class QtDebugClient(QMainWindow):
	def __init__(self, config):
		super(QtDebugClient, self).__init__()
		self.config = config
		
		# TODO: enable logger to log to a file specified via CLI
		self.log = logging.getLogger('wiab.QtDebugClient')
		
		self.assets = Assets(self.config)
		self.game = QtClientGame(self.config, self.assets)  # TODO make sure size gets set correctly later
		self.active_player = None  # TODO remove this?
		self.active_thing_id = None
		self.requested_player_name = None
		
		self.socket = QTcpSocket()
		self.data_to_process = b''
		self.emitters = {b"clear_things_s2c": self.received_clear_things, b"current_turn_s2c": self.received_current_turn, b"end_turn_done_s2c": self.received_end_turn_done, b"end_turn_not_ready_s2c": self.received_end_turn_not_ready, b"end_turn_ready_s2c": self.received_end_turn_ready, b"end_turn_start_s2c": self.received_end_turn_start, b"new_player_s2c": self.received_new_player, b"player_storage_s2c": self.received_player_storage, b"start_new_game_s2c": self.received_start_new_game, b"thing_info_s2c": self.received_thing_info}
		# TODO remove self.socket = QTcpSocket()
		# TODO remove self.socket.setblocking(False)
		
		self.tab_widget = QTabWidget()
		self.setCentralWidget(self.tab_widget)
		
		tab_map = QtTabMap(self, self.assets)
		tab_map.map_clicked.connect(self.map_clicked)
		self.tab_widget.addTab(tab_map.scroller, "Map")
		self.tabs = {"map": tab_map}
		
		tab_players = QtTabPlayers(self)
		self.tab_widget.addTab(tab_players.scroller, "Players")
		self.tabs["players"] = tab_players
		
		tab_things = QtTabThings(self)
		self.tab_widget.addTab(tab_things.scroller, "Things")
		self.tabs["things"] = tab_things
		
		self.tab_widget.setCurrentIndex(0)
		
		self.status_bar = QStatusBar(self)
		self.setStatusBar(self.status_bar)
		
		self.setup_menu_bar()
		# self.show()
		self.showMaximized()
	
	def cancel_action_points(self):  # This is intentionally client-side only. The point is to let the user skip the currently selected thing for select_actionable_thing. # TODO check that pylint notices unused methods like this (if this is still unused, else just make one)
		if self.selected_thing_id is not None:
			self.game.things[self.selected_thing_id].action_points = 0.0
			self.select_actionable_thing()
		else:
			self.log.warning("You need to select a thing to be able to cancel action points.")
	
	def closeEvent(self, event=None):  # have to use bad style name because we're overriding a method, same for unused param # pylint: disable=invalid-name,unused-argument
		self.log.info("quitting normally")
		self.socket.close()
		sys.exit(0)
	
	def connect_dia(self):
		dia = QtDiaConnect(self)
		dia.connect_clicked.connect(self.connect_dia_clicked)
		dia.exec()
	
	def connect_dia_clicked(self, host, port, player_name):
		self.requested_player_name = player_name
		self.log.debug("connect_dia_connect_clicked %s %d %s", host, port, player_name)
		self.socket.connected.connect(self.connected)
		self.socket.connectToHost(host, port)
	
	def connected(self):
		# self.log.debug("connected")
		self.socket.readyRead.connect(self.on_ready_read)
		self.send("connect_c2s", self.requested_player_name)
		self.refresh_status_bar()
	
	def end_turn_not_ready(self):
		self.send("end_turn_not_ready_c2s")
	
	def end_turn_ready(self):
		self.send("end_turn_ready_c2s")
	
	def log_tiles(self):
		self.log.debug(self.game.tiles)
	
	def map_clicked(self, tile_x, tile_y, button_code):
		self.log.debug("client.map_clicked at %d,%d, code %d", tile_x, tile_y, button_code)
		
		if button_code == Qt.LeftButton:
			if len(self.game.tiles[tile_x][tile_y].things) > 0:
				self.active_thing_id = self.game.tiles[tile_x][tile_y].things[0].thing_id  # TODO handle if multiple things on one tile
				self.refresh_status_bar()
		else:
			self.log.warning("client.map_clicked unrecognised button code at %d,%d, code %d", tile_x, tile_y, button_code)
	
	def move(self, delta_x, delta_y):
		if self.active_thing_id:
			target_location = self.game.get_location_offset(self.game.things[self.active_thing_id].location, (delta_x, delta_y))
			self.send("move_c2s", (target_location[0], target_location[1]), self.active_thing_id)
		else:
			self.log.warning("please select something before trying to move it")
	
	def move_e(self):
		self.move(1, 1)
	
	def move_n(self):
		self.move(-1, 1)
	
	def move_ne(self):
		self.move(0, 1)
	
	def move_nw(self):
		self.move(-1, 0)
	
	def move_s(self):
		self.move(1, -1)
	
	def move_se(self):
		self.move(1, 0)
	
	def move_sw(self):
		self.move(0, -1)
	
	def move_w(self):
		self.move(-1, -1)
	
	def new_game_dia(self):
		# if self.socket.state == QAbstractSocket.ConnectedState: #TODO why doesnt this work?
			dia = QtDiaNewGame(self)
			dia.new_game_clicked.connect(self.new_game_dia_clicked)
			dia.exec()
	
	def new_game_dia_clicked(self, size_x, size_y):
		self.send("start_new_game_c2s", (size_x, size_y))
	
	def on_ready_read(self, sender=None, recursively_called=False):
		if not recursively_called:
			self.data_to_process += self.sender().readAll().data()
			sender = self.sender()
		# self.log.debug("to process:", self.data_to_process)
		if len(self.data_to_process) < 4:
			return  # didnt receive length field yet
		
		length = int.from_bytes(self.data_to_process[:4], byteorder='big')
		
		if len(self.data_to_process) < length:
			return  # didnt receive full message yet
		
		message = msgpack.unpackb(self.data_to_process[4:4 + length])
		
		self.emitters[message[0]](message)
		
		self.data_to_process = self.data_to_process[4 + length:]
		if len(self.data_to_process) > 0:
			self.on_ready_read(sender=sender, recursively_called=True)
	
	def received_clear_things(self, message):  # intentionally unused param message # pylint: disable=unused-argument
		self.active_thing_id = None
		self.game.things = {}
		self.refresh_status_bar()
	
	def received_current_turn(self, message):
		self.game.current_turn = message[1]
	
	def received_end_turn_start(self, message):  # intentionally unused param message # pylint: disable=unused-argument
		self.tabs['map'].allow_draw = False  # TODO show user that end_turn is running
	
	def received_end_turn_done(self, message):  # intentionally unused param message # pylint: disable=unused-argument
		self.tabs['map'].allow_draw = True
		self.game.current_turn += 1
		self.tabs['map'].draw()
		self.log.debug("finished Client.received_end_turn_done current_turn=%d", self.game.current_turn)
		self.refresh_status_bar()
	
	def received_end_turn_not_ready(self, message):
		self.game.players[message[1]].end_turn_ready = False
	
	def received_end_turn_ready(self, message):
		self.game.players[message[1]].end_turn_ready = True
	
	def received_new_player(self, message):
		player_id = message[1]
		player_name = message[2].decode("UTF8")
		self.game.add_player(player_name, player_id)
		if player_name == self.requested_player_name:
			self.active_player = self.game.players[player_id]
			self.log.info("connected as %s with ID %d", self.active_player.name, self.active_player.player_id)
		self.refresh_status_bar()
		self.tabs["players"].draw()
	
	def received_player_storage(self, message):
		received_storage = ModelStorage(message[1], message[2])
		for key, value in message[3]:
			received_storage.contents[key.decode('UTF8')] = value
		self.active_player.storage = received_storage
		self.log.debug("received_player_storage: %s", str(received_storage))

	def received_start_new_game(self, message):
		self.log.info("received_start_new_game with the following data: %s, starting new game", str(message))
		self.game.start_game(message[1])
		self.tabs["map"].draw()
		self.tabs["players"].draw()
	
	def received_thing_info(self, message):
		thing = ModelThing(message[1], message[2], message[3].decode("UTF8"), message[4], message[5])
		#print("received thing with id %d" % message[5])
		self.game.things[message[5]] = thing
		self.tabs['things'].draw()
	
	def redraw_map(self):  # TODO can i give this directly in the menu?
		self.tabs["map"].draw()
	
	def refresh_status_bar(self):
		message = "connected to %s:%d" % (self.socket.peerName(), self.socket.peerPort())
		
		if self.active_player:
			message += " as %s." % self.active_player.name
		else:
			message += "."
		
		message += "Turn %d" % self.game.current_turn
		
		if self.active_thing_id:
			message += "Selected: " + self.game.things[self.active_thing_id].status_bar_string() + "."
		
		
		self.status_bar.showMessage(message)
	
	def send(self, *args):
		message = msgpack.packb(args)
		length = len(message).to_bytes(4, byteorder="big")
		self.socket.write(length + message)
		self.socket.flush()
		# self.log.debug("sent", length, message)
	
	def setup_menu_bar(self):
		self.menu_bar = QMenuBar(self)
		self.setMenuBar(self.menu_bar)
		
		self.menu_game = QMenu("Game", self.menu_bar)
		self.menu_game.addAction("Connect", self.connect_dia, QKeySequence("Ctrl+C"))
		self.menu_game.addAction("New Game", self.new_game_dia, QKeySequence("Ctrl+N"))
		self.menu_game.addSeparator()
		self.menu_game.addAction(QIcon.fromTheme("application-exit"), "Quit", self.closeEvent, QKeySequence("Ctrl+Q"))
		self.menu_bar.addAction(self.menu_game.menuAction())
		
		self.menu_turn = QMenu("Turn", self.menu_bar)
		self.menu_turn.addAction("Ready for Turn", self.end_turn_ready, QKeySequence("Ctrl+T"))
		self.menu_turn.addAction("Not Ready for Turn", self.end_turn_not_ready, QKeySequence("Ctrl+Shift+T"))
		self.menu_bar.addAction(self.menu_turn.menuAction())
		
		self.menu_unit = QMenu("Unit", self.menu_bar)
		self.menu_unit.addAction("Move Northwest", self.move_nw, QKeySequence("7"))
		self.menu_unit.addAction("Move North", self.move_n, QKeySequence("8"))
		self.menu_unit.addAction("Move Northeast", self.move_ne, QKeySequence("9"))
		self.menu_unit.addAction("Move West", self.move_w, QKeySequence("4"))
		self.menu_unit.addAction("Move East", self.move_e, QKeySequence("6"))
		self.menu_unit.addAction("Move Southwest", self.move_sw, QKeySequence("1"))
		self.menu_unit.addAction("Move South", self.move_s, QKeySequence("2"))
		self.menu_unit.addAction("Move Southeast", self.move_se, QKeySequence("3"))
		self.menu_bar.addAction(self.menu_unit.menuAction())
		
		self.menu_debug = QMenu("Debug", self.menu_bar)
		self.menu_debug.addAction("Log Full Map Details", self.log_tiles)
		self.menu_debug.addAction("Redraw Map", self.redraw_map)
		self.menu_bar.addAction(self.menu_debug.menuAction())
