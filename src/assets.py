# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
from configparser import ConfigParser
import logging
import os
import copy

# other libs

# own modules


class ThingType:
	pass


class DuplicateKeyError(Exception):
	pass


class Assets:
	def __init__(self, config):
		asset_directory = config.run.asset_directory
		
		self.log = logging.getLogger('wiab.assets')
		
		buildings_parser = ConfigParser()
		buildings_parser.read(os.path.join(asset_directory, 'buildings.ini'))
		
		units_parser = ConfigParser()
		units_parser.read(os.path.join(asset_directory, 'units.ini'))
		
		recipes_parser = ConfigParser()
		recipes_parser.read(os.path.join(asset_directory, 'recipes.ini'))
		
		items_parser = ConfigParser()
		items_parser.read(os.path.join(asset_directory, 'items.ini'))
		
		# TODO: use the extended configparser from start.py
		# TODO: convert other in a nicer form as well
		self.other = ConfigParser()
		self.other.read(os.path.join(asset_directory, 'other.ini'))
		
		self.types = {}  # {thing_type_name: ThingType}
		self.buildings = self._fill_types_dict(buildings_parser, 'building')
		self.units = self._fill_types_dict(units_parser, 'unit')
		self.recipes = self._fill_types_dict(recipes_parser, 'recipe')
		self.items = self._fill_types_dict(items_parser, 'item')
	
	def _fill_types_dict(self, config_parser, category):
		# build template
		template = ThingType()
		for value in config_parser['defaults']:
			template.__dict__[value] = config_parser['defaults'][value]
		
		# generate types from ini file read by/saved in config parser
		ret = {}
		for section in config_parser:
			if section != 'defaults' and section != 'DEFAULT':  # ignore our own and configparser's default section
				# raise exception if a key is already present
				if section in self.types:
					raise DuplicateKeyError(section)
				
				# make a copy of the template
				self.types[section] = copy.deepcopy(template)
				
				# fill the copy with the values in the ini file for that section
				for value in config_parser[section]:
					self.types[section].__dict__[value] = config_parser[section][value]
				
				self.types[section].name = section
				self.types[section].category = category
				ret[section] = self.types[section]
		
		return ret
	
	def get_thing_type(self, thing_type_name: [str, bytes]) -> ThingType:
		if isinstance(thing_type_name, bytes):
			thing_type_name = thing_type_name.decode('utf-8')
		return self.types[thing_type_name]
