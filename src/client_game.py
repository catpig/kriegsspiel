# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
import logging
from typing import Tuple

# other libs

# own modules
from model_game import ModelGame
from model_storage import ModelStorage
from helpers import CustomDict
from client_network import ClientNetwork
from client_world import ClientWorld
from player import Player
from assets import Assets
from client_thing import ClientThing


class ClientGame(ModelGame):
	"""
	Like ModelGame but specific to one Player.
	"""
	
	def __init__(self, config: CustomDict, network: ClientNetwork, world: ClientWorld,
	             size: [Tuple[float, float], None], assets: [Assets, None], player: Player):
		super(ClientGame, self).__init__(size, assets)
		self.log = logging.getLogger('wiab.client.game')
		
		self.config = config
		self.network = network
		self.world = world
		self.player = player
		
		self.players = {}  # {player_id: Player}
		self.things = {}  # {thing_id: Thing}
		self.assets = Assets(config)
		
		self.network.set_emitters({b"clear_things_s2c": self.received_clear_things,
		                           b"current_turn_s2c": self.received_current_turn,
		                           b"end_turn_done_s2c": self.received_end_turn_done,
		                           b"end_turn_not_ready_s2c": self.received_end_turn_not_ready,
		                           b"end_turn_ready_s2c": self.received_end_turn_ready,
		                           b"end_turn_start_s2c": self.received_end_turn_start,
		                           b"new_player_s2c": self.received_new_player,
		                           b"player_storage_s2c": self.received_player_storage,
		                           b"start_new_game_s2c": self.received_start_new_game,
		                           b"thing_info_s2c": self.received_thing_info})
		self.world.floor.set_height_callable(self.get_height)
		self.world.floor.set_color_callable(self.get_color)
		
		self.start_game()
		self.network.send("connect_c2s", self.player.name)
		
		if self.config.client.start_game:
			self.network.send("start_new_game_c2s", (self.config.client.world_size_request_x,
			                                         self.config.client.world_size_request_y))
	
	# i don't understand how, but apparently pylint is wrong when claiming that task in unused param
	def get_color(self, location: Tuple[float, float]) -> Tuple[float, float, float, float]:  # pylint: disable=unused-argument
		return self.config.client.background_color
	
	# TODO: find a way to have this as an instance variable instead of a class variable
	@property
	def size(self):
		return self._size
	
	@size.setter
	def size(self, new_size):
		self._size = new_size
		self.world.floor.set_size(new_size)
	
	def received_clear_things(self, message):  # param intentionally unused # pylint: disable=unused-argument
		self.things = {}
	
	def received_current_turn(self, message):
		self.current_turn = message[1]
	
	def received_end_turn_start(self, message):
		pass
	
	def received_end_turn_done(self, message):  # param message is intentionally unused # pylint: disable=unused-argument
		# this is called end_turn in the other classes, but I think consistency with the other received* methods is more important
		super(ClientGame, self).end_turn()
		self.log.debug("ClientGame.received_end_turn_done current_turn=%d", self.current_turn)
		for thing in self.things.values():
			thing.end_turn()
	
	def received_end_turn_not_ready(self, message):
		self.players[message[1]].end_turn_ready = False
	
	def received_end_turn_ready(self, message):
		self.players[message[1]].end_turn_ready = True
	
	def received_new_player(self, message):
		player_id = message[1]
		player_name = message[2].decode("UTF8")
		if player_name == self.player.name:
			self.player.player_id = player_id
			self.log.info("connected as %s with ID %d", self.player.name, self.player.player_id)
			
		else:
			self.log.debug('%s connected with ID %d', player_name, player_id)
		
		self.players[player_id] = Player(player_name, player_id)
	
	def received_player_storage(self, message):
		received_storage = ModelStorage(message[1], message[2])
		for key, value in message[3]:
			received_storage[key.decode('UTF8')] = value
		self.player.storage = received_storage

	def received_start_new_game(self, message):
		self.log.info("received_start_new_game with the following data: %s, starting new game", str(message))
		self.size = message[1]
		# TODO: fill self with some necessary info we get from the server like things, terrain and units
	
	def received_thing_info(self, message):
		thing = ClientThing(message[1], message[2], self.assets.get_thing_type(message[3]), message[4], message[5])
		self.things[message[5]] = thing
		self.world.place_thing(thing)
