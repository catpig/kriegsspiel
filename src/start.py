#!/usr/bin/env python3.5
# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

"""
start.py

One file to start them all. And to init the logger, read the .ini files and provide a usable command line interface.
It is supposed to be called via the command line with one or more of the following arguments:
	--debug
	
	--server
	--size x y
	
	--client
	--server_port port
	--server_ip ip

If you need help regarding the CLI arguments just call it via the CLI and there will be some hints.
"""

# import std lib
import argparse
import ast
import configparser
import getpass
import logging
import os
import signal
import sys

# other libs

# own modules
from server import Server
from client import Client
from helpers import CustomDict


def init():
	# run argparse
	config_cli = parse_arguments()
	
	# read configs and make user config if it doesn't exist
	config = configparser.ConfigParser(interpolation=None)
	config.read(os.path.join(os.path.dirname(sys.argv[0]), 'config_default.ini'))
	user_config_path = os.path.join(config_cli['run']['home_directory'], 'config_user.ini')
	if os.path.isfile(user_config_path):
		config.read(user_config_path)
	else:
		make_user_config(user_config_path)
	
	# convert config to a normal dict as config is unable to handle anything other than strings
	config_dict = convert_to_dict(config)
	# convert types
	config_dict = convert_types(config_dict)
	
	# integrate argparse into config_dict
	for cli_dict in config_cli:
		if cli_dict in config_dict:
			config_dict[cli_dict].update(config_cli[cli_dict])
		else:
			config_dict[cli_dict] = config_cli[cli_dict]
	
	# generate default paths and create dirs if necessary
	if os.name == 'posix':
		config_dict['server']['home_wiab_path'] = os.path.expanduser('~/.wiab')
	else:
		config_dict['server']['home_wiab_path'] = os.path.join(os.getenv('APPDATA'), 'wiab')
	config_dict['server']['save_path'] = os.path.join(config_dict['server']['home_wiab_path'], 'save')
	
	if not os.path.exists(config_dict['server']['home_wiab_path']):
		os.mkdir(config_dict['server']['home_wiab_path'])
	if not os.path.exists(config_dict['server']['save_path']):
		os.mkdir(config_dict['server']['save_path'])
	
	start_server = config_cli['run']['server']
	start_client = config_cli['run']['client']
	start_qt_client = config_cli['run']['qt_client']
	
	# convert config_dict to custom_dict for nicer access
	config_final = CustomDict(config_dict)
	
	# TODO: enable logger to log to a file specified via CLI
	logging.basicConfig(format='%(levelname)s %(asctime)s %(filename)s: %(message)s', level=config_final.general.log_level)
	log = logging.getLogger('wiab')
	log.info('logging started')
	log.debug('config_final: ' + str(config_final))
	
	if start_server and start_client:
		log.critical('You cannot start the client and the server at the same time')
	elif not start_server and not start_client and not start_qt_client:
		log.critical('You have to start either the client or the server, please give either --client or --server')
	elif start_server:
		log.info('starting server')
		try:
			server = Server(config_final)
			signal.signal(signal.SIGINT, signal.SIG_DFL)  # makes ctrl+C at the shell work
			server.exec_()
		except:  # we want a bare except in this line # pylint: disable=bare-except
			log.exception('the server crashed with the following message:\n')
	elif start_client:
		log.info('starting client')
		try:
			client = Client(config_final)
			client.run()
		except:  # we want a bare except in this line # pylint: disable=bare-except
			log.exception('the client crashed with the following message:\n')
	elif start_qt_client:
		log.info('starting qt client')
		try:
			import PyQt5  # having these down here so qt isn't necessary to run the starter
			from qt_debug_client import QtDebugClient
			
			app = PyQt5.QtWidgets.QApplication(sys.argv)
			signal.signal(signal.SIGINT, signal.SIG_DFL)  # makes ctrl+C at the shell work
			QtDebugClient(config_final)
			sys.exit(app.exec_())
		except:  # we want a bare except in this line # pylint: disable=bare-except
			log.exception('the client crashed with the following message:\n')
	
	log.info('finished, shutting down')
	logging.shutdown()


def parse_arguments():
	parser = argparse.ArgumentParser(description='CLI for wiab',
	                                 epilog='You have to start client and server separately.')
	parser.add_argument('-d', '--debug', help='enable debug mode and log lots of things', action='store_true')
	parser.add_argument('-v', '--verbose', help='like --debug but logs everything', action='store_true')
	
	parser.add_argument('-s', '--server', help='start the server', action='store_true')
	
	parser.add_argument('-c', '--client', help='start the client', action='store_true')
	parser.add_argument('-q', '--qt_client', help='start the qt client for debugging', action='store_true')
	parser.add_argument('--start_game', help='start a game after connecting to the server', action='store_true')
	parser.add_argument('--size', help='size of the map, x and y', nargs=2)
	parser.add_argument('-p', '--player_name', help='the name of the player', default=getpass.getuser())
	parser.add_argument('--asset_directory', help='TODO', default=os.path.join('..', 'asset_packs', 'default'))
	parser.add_argument('--home_directory', help='directory with user data such as config_user.ini', default='.')
	parser.add_argument('--server_host', help='host or IP of the server')
	parser.add_argument('--server_port', help='TCP port of the server (0-65535)')
	
	args = parser.parse_args()
	
	ret = {'general': {}, 'network': {}, 'server': {}, 'client': {}, 'run': {}}
	if args.debug or args.verbose:
		ret['general']['log_level'] = 'DEBUG'
		ret['general']['debug_mode'] = True
	if args.verbose:
		ret['general']['debug_verbose'] = True
	
	ret['run']['server'] = args.server
	ret['run']['asset_directory'] = args.asset_directory
	ret['run']['home_directory'] = args.home_directory
	
	ret['run']['client'] = args.client
	ret['run']['qt_client'] = args.qt_client
	ret['client']['start_game'] = args.start_game
	if args.size is not None:
		ret['client']['world_size_request_x'] = int(args.size[0])
		ret['client']['world_size_request_y'] = int(args.size[1])
	ret['client']['player_name'] = args.player_name
	if args.server_host is not None:
		ret['network']['host'] = args.server_host
	if args.server_port is not None:
		ret['network']['port'] = int(args.server_port)
		if ret['network']['port'] < 0 or ret['Network']['port'] > 65535:
			raise ValueError('entered tcp port is not within the allow range (0-65535)')
	
	return ret


def convert_to_dict(config):
	config_dict = {}
	
	sections = config.sections()
	for section in sections:
		config_dict[section] = dict(config.items(section))
	
	return config_dict


def convert_types(config_dict):
	for section in config_dict:
		for option in config_dict[section]:
			config_dict[section][option] = convert_type(config_dict[section][option])
	
	return config_dict


def convert_type(x: str):
	# check for str (first and last characters are double or single quotes)
	if x[0] == x[-1] and x[0] in ['"', "'"]:
		return x[1:-1]
	
	# check for int or float
	elif x[0] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '+', '-']:
		# check for float
		if '.' in x:
			return float(x)
		else:
			return int(x)
	
	# check for list, tuple and dict
	elif x[0] in ['[', '(', '{']:
		return ast.literal_eval(x)
	
	# check for bool
	elif x.lower() in ['true', 't', 'yes', 'y']:
		return True
	elif x.lower() in ['false', 'f', 'no', 'n']:
		return False
	else:
		raise ValueError('could not parse ' + x + ', unidentifiable type')
	
	
def make_user_config(path):
	text = '# everything in here overwrites the corresponding setting from config_default.ini\n' \
	       '# this file will be generated if it does not exist\n' \
	       '\n' \
	       '# comments start with "#"\n' \
	       '# section headers look like [this]\n' \
	       '# the options have the following data types\n' \
	       '#     string = "foo"\n' \
	       '#     integer = 42\n' \
	       '#     float = 13.37\n' \
	       '#     list = [1, 2, 3]\n' \
	       '#     tuple = (4.0, 5.1, 6.2)\n' \
	       '#     dict = {"a": 7, "b": 8, "c": 9}\n' \
	       '#     boolean = True\n' \
	       '\n' \
	       '\n' \
	       '\n' \
	       '[network]\n' \
	       '#host = "localhost"\n' \
	       '#port = 49000\n'
	file = open(path, 'w')
	file.write(text)
	file.close()


if __name__ == '__main__':
	init()
