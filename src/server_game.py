# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
import logging
import math
import os
import random
import sys
from typing import Dict, Tuple, Union

# other libs
import msgpack
from PyQt5.QtNetwork import QTcpSocket

# own modules
from assets import Assets
from wiab_errors import OutsideMap, WrongTerrainType
from helpers import CustomDict
from model_game import ModelGame
from model_storage import ModelStorage
from server_thing import ServerThing
from player import Player


class ServerGame(ModelGame):
	def __init__(self, config: CustomDict, size: Tuple[float, float], client_socket_dicts: Dict[int, Dict[str, Union[QTcpSocket, int, str]]], only_for_loading: bool=False):
		self.assets = Assets(config)
		super(ServerGame, self).__init__(size, self.assets)
		
		self.log = logging.getLogger('wiab.server.game')
		self.config = config
		
		for socket_dict in client_socket_dicts.values():
			self.players[socket_dict["player_id"]] = Player(socket_dict["player_name"], socket_dict["player_id"])
			self.players[socket_dict["player_id"]].human = True
		
		if only_for_loading:
			return
		
		# self.create_terrain_points()
		self.start_game()
		self.place_random_buildings()
		self.place_player_starting_things()
	
	def end_turn(self, send_all):  # difference in params to overridden method is intentional as ServerGame will need to send # pylint: disable=arguments-differ
		super(ServerGame, self).end_turn()
		self.log.debug("ServerGame.end_turn current_turn=%d", self.current_turn)
		for thing in self.things.values():
			thing.end_turn(send_all)
	
	def make_and_place_new_thing(self, owner_id: int, location: Tuple[float, float], thing_type_name: str):  # TODO add building_thing_id param and check that owner/requestor also owns the building_thing
		self.log.debug("trying to create new %s at %f,%f for %d", thing_type_name, location[0], location[1], owner_id)
		if location[0] > self.size[0] or location[1] > self.size[1] or location[0] < 0 or location[1] < 0:
			raise OutsideMap("invalid location given to ServerGame.make_and_place_new_thing: %f,%f" % (location[0], location[1]))
		
		# TODO check if the area is actually free
		# TODO check for appropriate terrain type
		
		new_thing_type = self.assets.get_thing_type(thing_type_name)
		new_thing = ServerThing(owner_id, location, new_thing_type, float(new_thing_type.action_points))  # only initialise the thing now that we know that we can place it
		self.things[new_thing.thing_id] = new_thing
		return new_thing.thing_id
	
	def move(self, thing_id, new_location, sender_id):  # TODO check: movement points, terrain type, terrain owner, whether way and/or target location are occupied
		self.things[thing_id].location = new_location
	
	def place_imprecise(self, player_id, location, thing_type_name):  # This method places things when the exact location doesn't matter. TODO make it better
		thing_placed = False
		while not thing_placed:
			try:
				self.make_and_place_new_thing(player_id, location, thing_type_name)
				thing_placed = True
				location = (location[0] + 1.0, location[1])
			except OutsideMap:
				location = (location[0] - 1.0, location[1] - 1.0)
				self.make_and_place_new_thing(player_id, location, thing_type_name)
				thing_placed = True
				location = (location[0] - 1.0, location[1] - 1.0)
			except WrongTerrainType:
				location = (location[0], location[1] + 1.0)
				self.make_and_place_new_thing(player_id, location, thing_type_name)
				thing_placed = True
				location = (location[0], location[1] + 1.0)
		return location
	
	def place_player_starting_things(self):
		for player_id in self.players.keys():
			if player_id == 0:
				continue
			location = (self.size[0] * random.random(), self.size[1] * random.random())
			for thing_type_name in self.assets.other['starting_things'].keys():
				location = self.place_imprecise(player_id, location, thing_type_name)
	
	def place_random_buildings(self):
		total_size = self.size[0] * self.size[1]
		for building in self.assets.buildings.values():
			if building.random_frequency == '0.0':
				continue
			self.log.debug('placing ' + building.name)
			number_to_place = math.ceil(total_size / float(building.random_frequency))
			for i in range(number_to_place):  # pylint doesnt like this i dont know a better way TODO find out better way # pylint: disable=unused-variable
				location = (self.size[0] * random.random(), self.size[1] * random.random())
				location = self.place_imprecise(0, location, building.name)
	
	def quick_load(self):
		# prep path
		load_path = os.path.join(self.config.server.save_path, 'quicksave.wiab_save')
		if not os.path.exists(load_path):
			self.log.warning("can't load, file %s doesn't exist", load_path)
			return
			# TODO tell clients about this
		self.log.debug("loading from %s", load_path)
		
		# read file, convert from msgpack
		with open(load_path, 'rb') as load_file:
			loaded_dict = msgpack.unpack(load_file)
		
		# check if asset path matches # TODO better way of ensuring asset pack is compatible (ie. put name and version into asset metadata)
		if loaded_dict[b'asset_path'].decode('UTF8') != self.config.run.asset_directory:
			self.log.warning("not loading due to differing asset paths: %s %s", loaded_dict[b'asset_path'].decode('UTF8'), self.config.run.asset_directory)  # TODO tell clients
			return
		
		# load size
		self.current_turn = loaded_dict[b'current_turn']
		ServerThing.next_thing_id = loaded_dict[b'next_thing_id']
		self.size = loaded_dict[b'size']
		
		# load players
		for player_id, loaded_player_dict in loaded_dict[b'players'].items():
			if player_id not in self.players.keys():
				self.log.debug("not loading due to player id missing: %d", player_id)  # TODO tell clients
				sys.exit(1)  # TODO if we do all checks before messing around with data we could simply show an error message
				return
			
			if loaded_player_dict[b'name'].decode('UTF8') != self.players[player_id].name:
				self.log.debug("not loading due to player name mismatch: %s %s", loaded_player_dict[b'name'].decode('UTF8'), self.players[player_id].name)  # TODO tell clients
				sys.exit(1)  # TODO if we do all checks before messing around with data we could simply show an error message
				return
			
			for diplo_player_id, diplo_state in loaded_player_dict[b'diplomatic_states'].items():
				self.players[player_id].diplomatic_states[diplo_player_id] = diplo_state.decode('UTF8')
			
			self.players[player_id].end_turn_ready = loaded_player_dict[b'end_turn_ready']
			self.players[player_id].human = loaded_player_dict[b'human']
			
			loaded_storage = ModelStorage(loaded_player_dict[b'storage'][b'volume_per_item'], loaded_player_dict[b'storage'][b'volume_total'])
			for storage_item, storage_amount in loaded_player_dict[b'storage'][b'contents'].items():
				loaded_storage[storage_item.decode('UTF8')] = storage_amount
			
			self.players[player_id].storage = loaded_storage
		
		# load things # TODO not nearly enough verification done on things
		for thing_id, loaded_thing_dict in loaded_dict[b'things'].items():
			loaded_thing = ServerThing(loaded_thing_dict[b'owner_id'], loaded_thing_dict[b'location'], self.assets.get_thing_type(loaded_thing_dict[b'thing_type']),
			                           loaded_thing_dict[b'action_points'], thing_id)
			self.things[thing_id] = loaded_thing
		
		self.log.debug("finished loading from %s", load_path)
	
	def quick_save(self):  # TODO add save format version
		# prep path
		# save_path = os.path.join(self.config.server.save_path, datetime.now().strftime('%Y%m%d_%H%M%S.%f') + '.wiab_save')
		save_path = os.path.join(self.config.server.save_path, 'quicksave.wiab_save')
		self.log.debug("saving to %s", save_path)
		
		# generate dict to save
		save_dict = {'asset_path': self.config.run.asset_directory, 'current_turn': self.current_turn, 'next_thing_id': ServerThing.next_thing_id, 'players': {}, 'size': self._size, 'things': {}}
		
		for player in self.players.values():
			save_dict['players'][player.player_id] = player.dict_for_save()
		
		for thing in self.things.values():
			save_dict['things'][thing.thing_id] = thing.dict_for_save()
		
		# convert to msgpack
		save_msg = msgpack.packb(save_dict)
		
		# TODO check for sufficient free space
		
		# write out file
		with open(save_path, 'wb') as save_file:
			save_file.write(save_msg)
		self.log.debug("finished saving to %s", save_path)
	
	# The following property is needed to expose self._size in a clean way.
	# This is necessary to be able to later bind events to any change of the size. # TODO is this needed on server?
	@property
	def size(self) -> Tuple[float, float]:
		return self._size
	
	@size.setter
	def size(self, new_size: Tuple[float, float]):
		self._size = new_size
