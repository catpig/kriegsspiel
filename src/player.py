# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# This class is for the game model's view of a player. It does not deal with connection related things.

# python std lib

# pyqt

# other libs

# own modules


class Player(object):
	def __init__(self, name: str, player_id=None):
		self.diplomatic_states = {}  # player_id:state (where state is "war", "peace_offered" or "peace")
		self.end_turn_ready = False
		self.human = False
		self.name = name
		self.player_id = player_id
		self.storage = None
	
	def dict_for_save(self):
		return {'diplomatic_states': self.diplomatic_states, 'end_turn_ready': self.end_turn_ready, 'human': self.human, 'name': self.name, 'storage': self.storage.dict_for_save()}
	
	def end_turn(self):
		self.end_turn_ready = False
