# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
import getpass

# pyqt
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtNetwork import QHostAddress
from PyQt5.QtWidgets import QDialog, QGridLayout, QLabel, QLineEdit, QPushButton

# other libs

# own modules


class QtDiaConnect(QDialog):
	connect_clicked = pyqtSignal(QHostAddress, int, str)
	
	def __init__(self, parent):
		super(QtDiaConnect, self).__init__(parent)
		
		self.setWindowModality(Qt.WindowModal)
		self.setWindowTitle("Connect")
		self.grid = QGridLayout(self)
		
		row_count = 0
		column_count = 0
		
		self.grid.addWidget(QLabel("Server Host Name/IP"), row_count, column_count)
		column_count += 1
		
		self.edit_host = QLineEdit("127.0.0.1")  # TODO load defaults from config
		self.grid.addWidget(self.edit_host, row_count, column_count)
		row_count += 1
		column_count = 0
		
		self.grid.addWidget(QLabel("Server Port"), row_count, column_count)
		column_count += 1
		
		self.edit_port = QLineEdit("49000")
		self.grid.addWidget(self.edit_port, row_count, column_count)
		row_count += 1
		column_count = 0
		
		self.grid.addWidget(QLabel("Player Name"), row_count, column_count)
		column_count += 1
		
		self.edit_name = QLineEdit(getpass.getuser())
		self.grid.addWidget(self.edit_name, row_count, column_count)
		row_count += 1
		column_count = 0
		
		button_cancel = QPushButton("Cancel")
		button_cancel.clicked.connect(self.close)
		self.grid.addWidget(button_cancel, row_count, column_count)
		column_count += 1
		
		button_connect = QPushButton("Connect")
		button_connect.clicked.connect(self.connect_button_clicked)
		self.grid.addWidget(button_connect, row_count, column_count)
		button_connect.setDefault(True)
	
	def connect_button_clicked(self):
		host = QHostAddress(self.edit_host.text())
		port = int(self.edit_port.text())
		player_name = self.edit_name.text()
		
		self.connect_clicked.emit(host, port, player_name)
		self.close()
