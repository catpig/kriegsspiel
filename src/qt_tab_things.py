# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib

# pyqt
from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QGridLayout, QLabel, QLayout, QScrollArea, QWidget

# other libs

# own modules


class QtTabThings(QObject):
	def __init__(self, parent):  # TODO find what parent did exactly and then apply it accordingly everywhere
		super(QtTabThings, self).__init__()
		self.parent = parent
		
		self.scroller = QScrollArea()
		self.widget = QWidget()
		self.main_grid = QGridLayout()
		self.main_grid.setSizeConstraint(QLayout.SetMinAndMaxSize)
		self.widget.setLayout(self.main_grid)
		self.scroller.setWidget(self.widget)
	
	def draw(self):
		while True:  # TODO move this part up to setting row_counter etc into GridScrollTab
			item = self.main_grid.takeAt(0)
			if not item:
				break
			item.widget().deleteLater()
		
		grid_x = 0
		grid_y = 0
		
		for header_item in ("ID", "owner", "type", "loc (x,y)"):
			self.main_grid.addWidget(QLabel(header_item), grid_y, grid_x, 1, 1)
			grid_x += 1
		grid_x = 0
		grid_y += 1
		
		for thing in self.parent.game.things.values():
			self.main_grid.addWidget(QLabel(str(thing.thing_id)), grid_y, grid_x, 1, 1)
			grid_x += 1
			
			self.main_grid.addWidget(QLabel(self.parent.game.players[thing.owner_id].name), grid_y, grid_x, 1, 1)
			grid_x += 1
			
			self.main_grid.addWidget(QLabel(thing.thing_type), grid_y, grid_x, 1, 1)
			grid_x += 1
			
			self.main_grid.addWidget(QLabel("%.2f, %.2f" % (thing.location[0], thing.location[1])), grid_y, grid_x, 1, 1)
			
			grid_x = 0
			grid_y += 1
