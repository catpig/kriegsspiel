# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# std lib
import logging
import math
import time
import random
from typing import List, Tuple, Callable

# other libs
import numpy as np
from panda3d.core import Geom, GeomNode
from panda3d.core import GeomVertexFormat, GeomVertexData, GeomVertexWriter
from panda3d.core import GeomTriangles, GeomLinestrips
from panda3d.core import PNMImage, Texture, SamplerState
from panda3d.core import NodePath, OrthographicLens, VBase4, AntialiasAttrib

# own modules
from helpers import adjust_to_multiple, adjust_to_power, make_divisible, remap, remap_parallel, distance_2d

# define a type hint for the vertex list
VertexList = List[Tuple[float, float, float]]  # type name should be capitalised # pylint: disable=invalid-name
Coord2D = Tuple[float, float]  # type name should be capitalised # pylint: disable=invalid-name
Coord3D = Tuple[float, float, float]  # type name should be capitalised # pylint: disable=invalid-name
Color = Tuple[float, float, float, float]  # type name should be capitalised # pylint: disable=invalid-name


class ClientWorldFloor:
	def __init__(self, config, render):
		self.config = config
		self.render = render
		
		self.size = None
		self.get_height = None
		self.get_color = None
		
		self.log = logging.getLogger('wiab.client.world.floor')
		
		# distance between rows (x) and columns (y)
		self.x_dist = None
		self.y_dist = None
		
		# get panda3d vertex format (for vertex and OpenGL style color).
		# for details see https://www.panda3d.org/manual/index.php/Pre-defined_vertex_formats
		self.vertex_format = GeomVertexFormat.getV3c4()
		self.vertex_format_texture = GeomVertexFormat.getV3t2()
		# self.render.set_render_mode_thickness(1.2)
		
		self.segments = {}  # {(x, y): nodePath}
		self.textures = {}  # {(x, y): FloorTexture}
	
	def set_size(self, size: Coord2D) -> None:
		self.size = size
		
		self.x_dist = self.config.client.floor_grid_size
		self.y_dist = self.x_dist * np.sqrt(3.0 / 4.0)  # for equilateral triangles
		
		# adjust distances between rows and columns to get the set size
		self.x_dist = make_divisible(self.x_dist, self.size[0])
		self.y_dist = make_divisible(self.y_dist, self.size[1])
		
		# TODO: remove previously drawn stuff
		start = time.time()
		self.draw_all_segments()
		self.log.debug('ClientWorldFloor.set_size() took %fs', time.time() - start)
	
	def set_height_callable(self, height_callable: Callable[[Coord2D], float]) -> None:
		self.get_height = height_callable
	
	def set_color_callable(self, color_callable: Callable[[Coord2D], Color]) -> None:
		self.get_color = color_callable
		
	def get_floor_height(self, position: Coord2D):
		"""
		Calculates the height of the floor on certain coordinates.
		
		:return: float
		"""
		
		points = self.get_encompassing_triangle(position)
		return self._get_height_between_points(position, points[0], points[1], points[2])
		
	def get_encompassing_triangle(self, position: Coord2D) -> Tuple[Coord3D, Coord3D, Coord3D]:
		"""
		Returns the 3 points defining the triangle around a given point.
		
		:param position:
		:return:
		"""
		
		x, y = position
		
		assert self.size is not None
		assert self.x_dist is not None and self.y_dist is not None
		assert 0 < x <= self.size[0] and 0 < y <= self.size[1]
		
		# determine if the lower row and column are odd
		x_id = math.floor(x / self.x_dist)
		y_id = math.floor(y / (self.y_dist / 2))
		odd_col = x_id % 2 != 0
		odd_row = y_id % 2 != 0
		
		# calculate coordinates of the encompassing triangle
		x_low = adjust_to_multiple(x, self.x_dist, floor=True)
		x_high = x_low + self.x_dist
		y_low = adjust_to_multiple(y, self.y_dist / 2, floor=True)
		y_high = y_low + self.y_dist / 2
		
		# assign the coordinates to the variables
		# p1 is always the highest, p2 is in the middle and p3 is the lowest
		if odd_row == odd_col:
			# bottom left to top right
			if ClientWorldFloor._is_in_upper_part(x, y, x_low, y_low, x_high, y_high, bottom_left_to_top_right=True):
				p1 = (x_low, y_low + self.y_dist)
				p2 = (x_high, y_high)
				p3 = (x_low, y_low)
			else:
				p1 = (x_high, y_high)
				p2 = (x_low, y_low)
				p3 = (x_high, y_high - self.y_dist)
		else:
			# top left to bottom right
			if ClientWorldFloor._is_in_upper_part(x, y, x_low, y_low, x_high, y_high, bottom_left_to_top_right=False):
				p1 = (x_high, y_low + self.y_dist)
				p2 = (x_low, y_high)
				p3 = (x_high, y_low)
			else:
				p1 = (x_low, y_high)
				p2 = (x_high, y_low)
				p3 = (x_low, y_high - self.y_dist)
		
		# adjust to borders
		p1 = (p1[0], min(p1[1], self.size[1]))
		p3 = (p3[0], max(p3[1], 0))
		
		# add height component
		p1 = (p1[0], p1[1], self.get_height(p1))
		p2 = (p2[0], p2[1], self.get_height(p2))
		p3 = (p3[0], p3[1], self.get_height(p3))
		
		return p1, p2, p3

	@classmethod
	def _is_in_upper_part(cls, x, y, xl, yl, xh, yh, bottom_left_to_top_right) -> bool:
		# normalise the coordinates that xl and yl are on (0, 0)
		x -= xl
		y -= yl
		xh -= xl
		yh -= yl
		
		# calculate linear function (y = m * x + n)
		if bottom_left_to_top_right:
			n = 0
			m = yh / xh
		else:
			n = yh
			m = -1 * (yh / xh)
		
		# determine if the given point is above the line. If it's on the line it will be counted as being below.
		return y > m * x + n
		
	def _get_height_between_points(self, position: Coord2D, pos1: Coord3D, pos2: Coord3D, pos3: Coord3D) -> float:
		"""
		Calculates the height of a point on the surface of a triangle defined by 3 points by splitting it into 2 2D problems.
		
		Only suitable for triangles where pos1 and pos3 are on the same x coordinate.
		
		:param position:
		:param pos1:
		:param pos2:
		:param pos3:
		:return:
		"""
		
		# ensure the x coordinate of the first and third point is the same
		assert pos1[0] == pos3[0]
		assert pos1[1] > pos3[1]
		
		# temporary point on the axis between pos1 and pos3 using a linear function through pos2 and position
		m = (position[1] - pos2[1]) / (position[0] - pos2[0])
		n = pos2[1] - m * pos2[0]
		y_tmp = m * pos1[0] + n
		z_tmp = remap(y_tmp, pos3[1], pos1[1], pos3[2], pos1[2])
		tmp = (pos1[0], y_tmp, z_tmp)
		
		# get the height of position
		return remap(distance_2d(tmp, position), 0, distance_2d(tmp, pos2[:2]), z_tmp, pos2[2])
	
	def draw_all_segments(self):
		assert self.size is not None
		
		amount_x = math.ceil(self.size[0] / self.config.client.floor_segment_size)
		amount_y = math.ceil(self.size[1] / self.config.client.floor_segment_size)
		for sy in range(amount_y):
			for sx in range(amount_x):
				self._add_segment((sx, sy))
	
	def _add_segment(self, segment: Tuple[int, int]) -> None:
		"""
		Add a segment to the scene graph.
		
		self.size has to be set via self.set_size() before calling this.
		How it works:
			
			y                   Each 'a' column is an odd_col and is displaced by -y_inc/2 with some correction for
			Δ                       this happening on the borders. Therefore b is at most of the same length as a.
			| b a b a b a
			| o   o   o         Every triangle is drawn on its own and a texture is generated for the whole segment
			|   o   o   o           and applied to the triangles.
			| o   o   o
			|   o   o   o
			| o   o   o
			|   o   o   o
			+------------> x
		
		:param segment: int tuple, (x, y) identifying the segment. has to consist of two positive integers
		"""
		'''
		some ideas for a possible rework in the future:
		- make a big list of the points in the grid and draw single triangles between them
			- how would that work in terms of performance? how would the vertex lists be used?
			- would it be easier to manipulate only some specific triangles/vertices?
			- would that be notably different from the current solution?
			- would likely make the code easier to read due to more modular tasks
		- use triangles in different shades of gray to display terrain instead of lines
			- how would we do terrain markers then?
			- could look more minimalistic
			- could make textures on the floor unnecessary
		'''
		
		assert self.size is not None
		assert self.get_height is not None and self.get_color is not None
		assert self.x_dist is not None and self.y_dist is not None
		
		# container for the vertex data
		triangles_vertex_data = GeomVertexData('floor triangles {}'.format(segment), self.vertex_format_texture, Geom.UHStatic)
		
		triangles = GeomTriangles(Geom.UHStatic)  # make a new primitive
		
		triangles_geom = Geom(triangles_vertex_data)  # make a Geom object and it's GeomNode that can be rendered
		
		triangles_geom.addPrimitive(triangles)  # add the primitives to the Geom object
		
		floor_node = GeomNode('floor node {}'.format(segment))  # create the GeomNode and add the Geoms to it
		floor_node.addGeom(triangles_geom)
		
		# attach the GeomNode to the render tree and save the node path in self.segments
		self.segments[segment] = self.render.attachNewNode(floor_node)
		
		# make vertex writers
		triangles_vertex = GeomVertexWriter(triangles_vertex_data, 'vertex')
		triangles_texcoord = GeomVertexWriter(triangles_vertex_data, 'texcoord')
		
		# calculate limits
		segment_size = self.config.client.floor_segment_size
		x_min = adjust_to_multiple(segment[0] * segment_size, self.x_dist)
		x_max = adjust_to_multiple((segment[0] + 1) * segment_size, self.x_dist)
		y_min = adjust_to_multiple(segment[1] * segment_size, self.y_dist / 2)
		y_max = adjust_to_multiple((segment[1] + 1) * segment_size, self.y_dist / 2) + self.y_dist / 2
		
		x_max = min(x_max, self.size[0])
		y_max = min(y_max, self.size[1])
		
		# generate empty texture and set it up for use
		self.textures[segment] = FloorTexture(self.config, x_min, y_min - self.y_dist / 2, x_max, y_max + self.y_dist / 2)
		self.segments[segment].setTexture(self.textures[segment].get_texture())
		
		# TODO: calculate this in advance for a big performance boost (~15ms vs ~23ms)
		triangles_vertex_data.set_num_rows(2000)
		
		last_col_tri = None
		odd_col = int(x_min / self.x_dist) % 2 != 0
		
		count_triangles = 0  # this one is necessary  # TODO: is this still necessary?
		
		x = x_min
		while x <= x_max:
			# build a column
			col_tri = []
			
			y = y_min
			if odd_col:
				if y == 0:
					col_tri.append((x, y, self.get_height((x, y))))
					y += 0.5 * self.y_dist
				else:
					y -= 0.5 * self.y_dist
					col_tri.append((x, y, self.get_height((x, y))))
					y += self.y_dist
			
			while y <= y_max + 0.000001:  # compensate for imprecise float operations
				col_tri.append((x, y, self.get_height((x, y))))
				y += self.y_dist
			
			if y_max == self.size[1] and y - 0.5 * self.y_dist <= self.size[1] + 0.000001:
				y -= 0.5 * self.y_dist
				col_tri.append((x, y, self.get_height((x, y))))
			
			# merge the vertex lists and draw them
			if last_col_tri is not None:
				# draw the triangles
				positions_tri = self._merge_vertex_lists(last_col_tri, col_tri, reverse=odd_col)
				amount = len(positions_tri)
				odd_tri = odd_col
				for i in range(2, amount):
					if not odd_tri:
						p1 = positions_tri[i - 2]
						p2 = positions_tri[i - 1]
						p3 = positions_tri[i]
					else:
						p1 = positions_tri[i - 1]
						p2 = positions_tri[i - 2]
						p3 = positions_tri[i]
						
					self.textures[segment].draw_triangle(p1[0:2], p2[0:2], p3[0:2])
					triangles_vertex.addData3f(*p1)
					triangles_texcoord.addData2f(*self.textures[segment].coord_world2tex(p1[0:2]))
					triangles_vertex.addData3f(*p2)
					triangles_texcoord.addData2f(*self.textures[segment].coord_world2tex(p2[0:2]))
					triangles_vertex.addData3f(*p3)
					triangles_texcoord.addData2f(*self.textures[segment].coord_world2tex(p3[0:2]))
					
					odd_tri = not odd_tri
				
				triangles.add_next_vertices((amount - 2) * 3)
				triangles.close_primitive()
				count_triangles += (amount - 2) * 3
			
			last_col_tri = col_tri
			odd_col = not odd_col
			x += self.x_dist
		# end while
		
		if self.config.general.debug_verbose:
			self.log.debug('segment (%d,%d): nr of vertices in triangles %d', segment[0], segment[1], count_triangles)
	
	def _merge_vertex_lists(self, a: VertexList, b: VertexList, reverse: bool) -> VertexList:
		"""
		Alternately merges two lists.
		
		:param a: first list
		:param b: second list
		:param reverse: whether to reverse the switch the lists before merging them
		:return: the merged list
		"""
		if reverse:
			a, b = b, a  # a and b are of equal length or b is smaller
		ret = []
		
		for i in range(0, len(b)):
			ret.append(a[i])
			ret.append(b[i])
		if len(a) > len(b):
			ret.append(a[len(b)])
			if len(a) - len(b) > 1:
				self.log.debug('_merge_vertex_lists(): list a is more than 1 element bigger than b, this may be a bug')
				self.log.debug('a bit of list a: %s', str(a[:10]))
				self.log.debug('a bit of list b: %s', str(b[:10]))
		
		return ret


class FloorTexture:
	"""
	Abstraction of the texture for one segment.
	
	It is not trying to reduce the amount of lines drawn as it may well be a lot more expensive than just drawing them
		a couple of times.
	"""
	
	scene_root = NodePath('Scene Floor Texture Root')
	
	def __init__(self, config, x_min, y_min, x_max, y_max):
		self.config = config
		self.log = logging.getLogger('wiab.client.world.floor.texture')
		
		# boundaries of the texture in world coordinates
		self.world_min = np.array([x_min, y_min])
		self.world_max = np.array([x_max, y_max])
		
		# size of the image
		img_size_x, img_size_y = x_max - x_min, y_max - y_min
		img_size_x *= self.config.client.floor_texture_resolution
		img_size_y *= self.config.client.floor_texture_resolution
		img_size_x = adjust_to_power(img_size_x, 2, floor=False)
		img_size_y = adjust_to_power(img_size_y, 2, floor=False)
		self.image_size = (img_size_x, img_size_y)
		self.log.debug('framebuffer size: (%i, %i)', img_size_x, img_size_y)
		
		# make texture buffer and texture, make and set up camera
		self.buffer = base.win.make_texture_buffer('Floor Buffer {}'.format(self.world_min), *self.image_size)
		self.buffer.set_clear_color(VBase4(random.random(), random.random(), random.random(), 1))
		self.buffer.set_sort(-100)  # increase render priority to render earlier than the main window
		self.buffer.set_one_shot(True)
		self.texture = self.buffer.get_texture()
		self.camera = base.make_camera(self.buffer)
		lens = OrthographicLens()
		lens.set_film_size(x_max - x_min, y_max - y_min)
		lens.set_near_far(-100, 100)
		self.camera.node().set_lens(lens)
		c_x = (x_min + x_max) / 2
		c_y = (y_min + y_max) / 2
		self.camera.set_pos(c_x, c_y, 0)
		self.camera.look_at(c_x, c_y, -1)
		
		# make scene and attach everything to it
		self.scene = NodePath('Scene Floor Texture {}'.format(self.world_min))
		self.scene.reparent_to(self.scene_root)
		self.camera.reparent_to(self.scene_root)
		self.line_vertex_data = GeomVertexData('floor texture lines {}'.format(self.world_min),
		                                       GeomVertexFormat.getV3c4(),
		                                       Geom.UH_static)
		self.line_vertex_data.set_num_rows(2000)  # TODO: calculate a realistic estimate
		self.line_vertices = GeomVertexWriter(self.line_vertex_data, 'vertex')
		self.line_color = GeomVertexWriter(self.line_vertex_data, 'color')
		self.line = GeomLinestrips(Geom.UH_static)
		line_geom = Geom(self.line_vertex_data)
		line_geom.add_primitive(self.line)
		line_geom_node = GeomNode('floor texture line gnode {}'.format(self.world_min))
		line_geom_node.add_geom(line_geom)
		self.line_node_path = self.scene.attach_new_node(line_geom_node)
		self.line_node_path.set_antialias(AntialiasAttrib.MLine)
		
		# boundaries of the texture in texture coordinates
		self.texture_boundaries = np.array([1.0, 1.0])  # boundaries for the texture coordinates
		
		# set texture options
		self.texture.set_wrap_u(Texture.WM_clamp)
		self.texture.set_wrap_v(Texture.WM_clamp)
		self.texture.set_magfilter(SamplerState.FT_linear)
		self.texture.set_minfilter(SamplerState.FT_linear_mipmap_linear)
		self.texture.set_anisotropic_degree(16)  # set amount of anisotropic filtering, has to be a power of 2
	
	def get_texture(self):
		return self.texture
		
	def draw_triangle(self, p1, p2, p3):
		"""
		Draw a triangle at the given points. All cordinates are world coordinates
		
		:param p1: first point
		:param p2: second point
		:param p3: third point
		"""
		
		self.line_vertices.add_data3f(p1[0], p1[1], 0)
		self.line_color.add_data4f(*self.config.client.floor_line_color)
		self.line_vertices.add_data3f(p2[0], p2[1], 0)
		self.line_color.add_data4f(*self.config.client.floor_line_color)
		self.line_vertices.add_data3f(p3[0], p3[1], 0)
		self.line_color.add_data4f(*self.config.client.floor_line_color)
		self.line_vertices.add_data3f(p1[0], p1[1], 0)
		self.line_color.add_data4f(*self.config.client.floor_line_color)
		
		self.line.add_next_vertices(4)
		self.line.close_primitive()
		pass
	
	def coord_world2tex(self, p):
		"""
		Translate world coordinates to texture coordinates.
		"""
		
		return remap_parallel(p, self.world_min, self.world_max, [0, 0], self.texture_boundaries)
	
	def coord_tex2world(self, p):
		"""
		Translate texture coordinates to world coordinates.
		"""

		return remap_parallel(p, [0, 0], self.texture_boundaries, self.world_min, self.world_max)
