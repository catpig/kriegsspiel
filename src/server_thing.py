# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.


# python std lib
from typing import Tuple

# other libs

# own modules
from model_thing import ModelThing
from assets import ThingType


class ServerThing(ModelThing):
	next_thing_id = 0
	
	def __init__(self, owner_id: int, location: Tuple[float, float], thing_type: ThingType, action_points: float, thing_id: int=None):
		super(ServerThing, self).__init__(owner_id, location, thing_type, action_points, thing_id)
		
		if thing_id is None:
			self.thing_id = ServerThing.next_thing_id
			ServerThing.next_thing_id += 1
		else:
			self.thing_id = thing_id
			if thing_id >= ServerThing.next_thing_id:
				ServerThing.next_thing_id = thing_id + 1
	
	def end_turn(self, send_all):  # difference in params to overridden method is intentional as ServerThing will need to send # pylint: disable=arguments-differ
		super(ServerThing, self).end_turn()
	
	def send(self, send_all):
		send_all("thing_info_s2c", self.owner_id, self.location, self.thing_type.name, self.action_points, self.thing_id)
