# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# std lib
import math
from typing import Tuple, List

# external libs
import numpy

Coord3D = [Tuple[float, float, float], List[float]]


def sgn(a: [float, int]):
	"""
	Implements the sign function. Returns 1 for positive inputs, -1 for negative inputs and 0 if its 0.
	
	:param a: the input
	:return: int, the sign of the input
	"""
	
	if a > 0:
		return 1
	elif a < 0:
		return -1
	return 0


def distance_2d(point_a, point_b):
	return math.sqrt(math.pow(point_b[0] - point_a[0], 2) + math.pow(point_b[1] - point_a[1], 2))


def remap(value: float, orig_min: float, orig_max: float, target_min: float, target_max: float):
	"""
	Remap a value from one range to another.
	
	:param value: the value
	:param orig_min: minimum of the values range
	:param orig_max: maximum of the values range
	:param target_min: minimum of the targets range
	:param target_max: maximum of the targets range
	:return: the remapped value
	"""
	
	orig_delta = orig_max - orig_min
	target_delta = target_max - target_min
	value -= orig_min
	value /= orig_delta
	value *= target_delta
	value += target_min
	return value


def remap_parallel(value: Coord3D, orig_min: Coord3D, orig_max: Coord3D, target_min: Coord3D, target_max: Coord3D):
	"""
	Does the same as remap but for multiple dimensions at the same time using numpy.
	
	:param value:
	:param orig_min:
	:param orig_max:
	:param target_min:
	:param target_max:
	:return:
	"""
	
	for i in range(len(value)):
		assert orig_min[i] <= value[i] <= orig_max[i]
	
	value = numpy.array(value)
	orig_min, orig_max = numpy.array(orig_min), numpy.array(orig_max)
	target_min, target_max = numpy.array(target_min), numpy.array(target_max)
	orig_delta = numpy.subtract(orig_max, orig_min)
	target_delta = numpy.subtract(target_max, target_min)
	
	value = numpy.subtract(value, orig_min)
	value = numpy.true_divide(value, orig_delta)
	value = numpy.multiply(value, target_delta)
	value = numpy.add(value, target_min)
	return value


def make_divisible(divisor: float, dividend: [int, float]) -> float:
	"""
	Adjust divisor so that base is divisible by divisor without (much) remainder.
	
	:param divisor:
	:param dividend:
	:return:
	"""
	
	dx = dividend / divisor
	dx = max(int(dx), 1)
	divisor = dividend / dx
	return divisor


def adjust_to_multiple(x: float, base: [int, float], floor=True):
	"""
	Adjust x to be a multiple of base.
	
	:param x:
	:param base:
	:param floor: can be None, True or False
	:return:
	"""
	x = x / base
	
	if floor is None:
		x = int(x)
	elif floor is True:
		x = math.floor(x)
	else:
		x = math.ceil(x)
	
	return x * base


def adjust_to_power(x: float, base: [int, float], floor=True):
	"""
	Adjust x to the next power of base.
	
	:param x:
	:param base:
	:param floor: can be None, True or False.
	:return:
	"""
	
	x = math.log(x, base)
	
	if floor is None:
		x = int(x)
	elif floor is True:
		x = math.floor(x)
	else:
		x = math.ceil(x)
		
	return int(math.pow(base, x))


def rotate_vector(v, angle: float, in_radians=False):
	"""
	rotate a 2d vector by a given angle

	:param v: 2-tupel (float) containing the vector
	:param angle: float
	:param in_radians: bool
	:return:
	"""
	if not in_radians:
		angle = angle * numpy.pi / 180.0
	
	try:
		x, y = v
		x2 = x * numpy.cos(angle) - y * numpy.sin(angle)
		y2 = x * numpy.sin(angle) + y * numpy.cos(angle)
		return numpy.array([x2, y2])
	except:
		raise TypeError('no rotatable vector given')


class CustomDict:
	# TODO: make this usable as a normal dict as well
	# TODO: write something nice to easily update this with another dict
	
	def __init__(self, d=None):
		if isinstance(d, dict):
			for x in d:
				self.__setattr__(x, d[x])
	
	def __setattr__(self, key, value):
		if isinstance(value, dict):
			super(CustomDict, self).__setattr__(key, CustomDict(value))  # automatic conversion from dict
		else:
			super(CustomDict, self).__setattr__(key, value)
	
	def __repr__(self):
		return self.__dict__.__repr__()


class FunctionsExecutor:
	"""
	A simple helper to bind functions to a key and call every function bound to that key in one go.
	You can bind multiple functions to the same key but their fid has to be unique for that key.
	"""
	
	def __init__(self):
		self._funcs = {}  # Format: {key: {fid: (function, [args], {kwargs}) }}
	
	def add(self, key, fid, function, *args, **kwargs):
		"""
		Bind a new function to a key.

		:param key: the key to call the function
		:param fid: id for this specific function, has to be unique only for that key and is need to remove the binding
		:param function: the function to be called
		:param args: unnamed/positional arguments
		:param kwargs: named arguments
		"""
		a = (function, args, kwargs)
		if key not in self._funcs:
			self._funcs[key] = {fid: a}
		else:
			self._funcs[key].update({fid: a})
	
	def call(self, key, silent=True):
		"""
		Call every function bound to key.

		:param key: the key the functions are bound to
		:param silent: default: True, whether to be silent on requests for unknown keys
		"""
		if key in self._funcs:
			for x in self._funcs[key]:
				self._funcs[key][x][0](*self._funcs[key][x][1], **self._funcs[key][x][2])
		elif not silent:
			raise KeyError('key {} is unknown'.format(key))
	
	def remove(self, key, fid):
		"""
		Unbind a function.
		Always raises an error if key or fid are unknown.

		:param key: the key the function is bound to
		:param fid: the ID of that specific function
		"""
		del self._funcs[key][fid]
	
	def check(self, key, fid=None):
		"""
		Checks if key is a known key and (optionally) if fid is known for that key.

		:param key: the key to be tested
		:param fid: optional, test if fid is known as well
		:return: boolean, whether key (or key and fid) has/have been registered/bound
		"""
		if key in self._funcs:
			if fid is None:
				return True
			else:
				return fid in self._funcs[key]
		else:
			return False
	
	def __repr__(self):
		# TODO: write __str__ to give a more readable output
		return self._funcs.__repr__()
