# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
import logging

# other libs

# own modules
from assets import Assets
from model_game import ModelGame
from player import Player


class QtClientGame(ModelGame):
	"""
	Much can't be shared with normal ClientGame due to totally different networking
	"""
	
	def __init__(self, config, assets: Assets):
		super(QtClientGame, self).__init__(None, assets)
		self.log = logging.getLogger('wiab.client.game')
	
	def add_player(self, name, player_id):
		self.players[player_id] = Player(name, player_id)
		for player in self.players.values():
			if player.player_id == player_id:
				pass
			player.diplomatic_states[player_id] = "unknown"
			self.players[player_id].diplomatic_states[player.player_id] = "unknown"
	
	def start_game(self, size):
		self.size = size
		super(QtClientGame, self).start_game()
	
