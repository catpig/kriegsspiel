# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
import logging

# other libs (for some reason pylint claimed the previous order was "wrong")
from panda3d.core import Geom, GeomNode, GeomTriangles
from panda3d.core import GeomVertexFormat, GeomVertexData, GeomVertexWriter
from direct.task import Task

# own modules
from client_world_floor import ClientWorldFloor
from helpers import CustomDict
from client_thing import ClientThing


class ClientWorld:
	"""
	This class abstracts the OpenGL world. It's sole purpose is to display the information the server generates.
	It only does something when called by ClientGame.
	"""
	
	def __init__(self, config: CustomDict, render):
		self.config = config
		self.render = render  # panda 3d renderer / scene graph
		
		self.log = logging.getLogger('wiab.client.world')
		self.floor = ClientWorldFloor(self.config, self.render.attach_new_node('floor'))
	
	# called by the panda task manager, shouldn't take too long to run. meant for world updates.
	def update(self, dt):  # pylint: disable=unused-argument,no-self-use
		# TODO: put world updates in here
		return Task.cont
	
	def place_thing(self, thing: ClientThing) -> None:
		if thing.thing_type.name == 'forest':
			color = (0, 0.5, 0.1, 1)
		elif thing.thing_type.category == 'unit':
			color = (.75, 0, 0, 1)
		else:
			color = (1, 1, 1, 1)
		
		if thing.thing_type.category == 'unit':
			size_xy = 3
			size_z = 6
		else:
			size_xy = 1
			size_z = 2
		
		# TODO: distinguish between ground units and flying units
		height = self.floor.get_floor_height(thing.location)
		
		self.draw_cube(thing, height, color, size_xy, size_z)
	
	def draw_cube(self, thing: ClientThing, height, color, size_xy=1, size_z=2) -> None:
		vformat = GeomVertexFormat.getV3c4()
		vdata = GeomVertexData('thing ' + str(thing.thing_id), vformat, Geom.UHStatic)
		triangles = GeomTriangles(Geom.UHStatic)
		geom = Geom(vdata)
		geom.addPrimitive(triangles)
		geomnode = GeomNode('thing' + str(thing.thing_id) + ' node')
		geomnode.addGeom(geom)
		self.render.attach_new_node(geomnode)
		
		vdata.set_num_rows(30)
		vwriter = GeomVertexWriter(vdata, 'vertex')
		cwriter = GeomVertexWriter(vdata, 'color')
		position_x, position_y = thing.location
		position_z = height
		dxy = size_xy / 2.0
		
		# first face in -x direction
		vwriter.addData3f(position_x - dxy, position_y + dxy, position_z)
		vwriter.addData3f(position_x - dxy, position_y - dxy, position_z)
		vwriter.addData3f(position_x - dxy, position_y + dxy, position_z + size_z)
		vwriter.addData3f(position_x - dxy, position_y + dxy, position_z + size_z)
		vwriter.addData3f(position_x - dxy, position_y - dxy, position_z)
		vwriter.addData3f(position_x - dxy, position_y - dxy, position_z + size_z)
		
		# second face in +y direction
		vwriter.addData3f(position_x + dxy, position_y + dxy, position_z)
		vwriter.addData3f(position_x - dxy, position_y + dxy, position_z)
		vwriter.addData3f(position_x + dxy, position_y + dxy, position_z + size_z)
		vwriter.addData3f(position_x + dxy, position_y + dxy, position_z + size_z)
		vwriter.addData3f(position_x - dxy, position_y + dxy, position_z)
		vwriter.addData3f(position_x - dxy, position_y + dxy, position_z + size_z)
		
		# third face in +x direction
		vwriter.addData3f(position_x + dxy, position_y - dxy, position_z)
		vwriter.addData3f(position_x + dxy, position_y + dxy, position_z)
		vwriter.addData3f(position_x + dxy, position_y - dxy, position_z + size_z)
		vwriter.addData3f(position_x + dxy, position_y - dxy, position_z + size_z)
		vwriter.addData3f(position_x + dxy, position_y + dxy, position_z)
		vwriter.addData3f(position_x + dxy, position_y + dxy, position_z + size_z)
		
		# fourth face in -y direction
		vwriter.addData3f(position_x - dxy, position_y - dxy, position_z)
		vwriter.addData3f(position_x + dxy, position_y - dxy, position_z)
		vwriter.addData3f(position_x - dxy, position_y - dxy, position_z + size_z)
		vwriter.addData3f(position_x - dxy, position_y - dxy, position_z + size_z)
		vwriter.addData3f(position_x + dxy, position_y - dxy, position_z)
		vwriter.addData3f(position_x + dxy, position_y - dxy, position_z + size_z)
		
		# top
		vwriter.addData3f(position_x + dxy, position_y + dxy, position_z + size_z)
		vwriter.addData3f(position_x - dxy, position_y + dxy, position_z + size_z)
		vwriter.addData3f(position_x + dxy, position_y - dxy, position_z + size_z)
		vwriter.addData3f(position_x + dxy, position_y - dxy, position_z + size_z)
		vwriter.addData3f(position_x - dxy, position_y + dxy, position_z + size_z)
		vwriter.addData3f(position_x - dxy, position_y - dxy, position_z + size_z)
		
		for i in range(30):  # pylint: disable=unused-variable
			cwriter.addData4f(*color)
		
		triangles.add_next_vertices(30)
