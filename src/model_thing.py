# -*- coding: utf8 -*-

# Copyright (C) 2015-2017 by the wiab contributors,
# see the git history and doc/contributors.txt for details.
# This file is part of wiab.
#
# wiab is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# wiab is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License
# along with wiab in doc/agpl_3.0.txt.
# If not, see <https://www.gnu.org/licenses/agpl.html>.

# python std lib
from typing import Tuple

# other libs

# own modules
from assets import ThingType


class ModelThing:
	def __init__(self, owner_id: int, location: Tuple[float, float], thing_type: ThingType, action_points: float=0.0, thing_id: int=None):
		self.owner_id = owner_id
		self.location = location
		self.thing_type = thing_type
		
		self.thing_id = thing_id
		
		self.action_points = action_points
	
	def dict_for_save(self):
		return {'action_points': self.action_points, 'location': self.location, 'owner_id': self.owner_id,
		        'thing_type': self.thing_type.name}
	
	def end_turn(self):
		self.action_points = float(self.thing_type.action_points)
